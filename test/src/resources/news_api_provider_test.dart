import 'dart:convert';

import 'package:flutter_news/src/models/item_model.dart';
import 'package:flutter_news/src/resources/news_api_provider.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';

void main() {
  test('fetchTopIds(): returns a list of ids', testFetchTopIds);
  test('fetchItem(): returns a ItemModel', testFetchItem);
}

testFetchTopIds() async {
  final newsApi = NewsApiProvider();
  newsApi.client = MockClient((Request request) async {
    final List<int> mockData = [
      9129911,
      9129199,
      9127761,
      9128141,
      9128264,
      9127792,
      9129248,
      9127092,
      9128367
    ];
    final String responseJson = json.encode(mockData);
    final Response response = Response(responseJson, 200);
    return response;
  });

  final List<dynamic> ids = await newsApi.fetchTopIds();
  expect(ids, [9129911, 9129199, 9127761, 9128141, 9128264, 9127792, 9129248, 9127092, 9128367]);
}

testFetchItem() async {
  final newsApi = NewsApiProvider();
  newsApi.client = MockClient((Request request) async {
    final Map<String, dynamic> jsonMap = {
      "by": "dhouston",
      "descendants": 71,
      "id": 9999,
      "kids": [8952, 9224, 8917],
      "score": 111,
      "time": 1175714200,
      "title": "My YC app: Dropbox - Throw away your USB drive",
      "type": "story",
      "url": "http://www.getdropbox.com/u/2/screencast.html"
    };
    final String responseJson = json.encode(jsonMap);
    final Response response = Response(responseJson, 200);
    return response;
  });

  final ItemModel item = await newsApi.fetchItem(9999);
  expect(item.id, 9999);
}
