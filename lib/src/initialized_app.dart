import 'package:flutter/material.dart';
import 'package:flutter_news/src/blocs/comments_bloc.dart';
import 'package:flutter_news/src/blocs/stories_bloc.dart';
import 'package:flutter_news/src/screens/news_detail_screen.dart';
import 'package:flutter_news/src/screens/news_list_screen.dart';
import 'package:provider/provider.dart';

class InitializedApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Provider<StoriesBloc>(
      create: (context) => StoriesBloc(),
      child: Provider<CommentsBloc>(
        create: (context) => CommentsBloc(),
        child: MaterialApp(
          title: 'Flutter News',
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          // home: NewsListScreen(),
          // if you want to make the route web like override this method
          // good place to make an initial data fetching etc...
          onGenerateRoute: _buildRoutes,
        ),
      ),
    );
  }

  Route<dynamic> _buildRoutes(RouteSettings settings) {
    if (settings.name == '/') {
      return MaterialPageRoute(
        builder: (BuildContext context) {
          // fetch initial data for top ids
          final _storiesBloc = Provider.of<StoriesBloc>(context);
          _storiesBloc.fetchTopIds();
          return NewsListScreen();
        },
      );
    } else {
      // extrat the item id from settings.name
      // pass id to NewsDetailScreen
      String sanitizedId = settings.name.replaceFirst('/', '');
      int id = int.parse(sanitizedId);
      return MaterialPageRoute(
        builder: (BuildContext context) {
          // fetch initial data for detail page
          final _commentsBloc = Provider.of<CommentsBloc>(context);
          _commentsBloc.fetchItemWithComments(id);
          return NewsDetailScreen(itemOf: id);
        },
      );
    }
  }
}
