import 'dart:async';
import 'package:flutter_news/src/resources/news_api_provider.dart';
import 'package:flutter_news/src/resources/news_db_provider.dart';
import 'package:flutter_news/src/models/item_model.dart';

class Repository {
  List<Source> sources = <Source>[
    newsDbProvider,
    NewsApiProvider(),
  ];

  List<Cache> caches = <Cache>[
    newsDbProvider,
  ];

  // TODO(joshua-hashimoto): iterate through all sources
  Future<List<int>> fetchTopIds() {
    // top ids are not stored in db.
    // cheaping out
    return sources[1].fetchTopIds();
  }

  Future<ItemModel> fetchItem(int id) async {
    ItemModel item;
    Source source;
    // the source outsite and the variable for for loop
    // referes to the same variable
    for (source in sources) {
      item = await source.fetchItem(id);
      if (item != null) {
        break;
      }
    }

    for (var cache in caches) {
      // check is item is already in the db
      if (cache != source) {
        cache.addItem(item);
      }
    }
    return item;
  }

  Future<void> clearCache() async {
    for (var cache in caches) {
      await cache.clear();
    }
  }
}

abstract class Source {
  Future<List<int>> fetchTopIds();
  Future<ItemModel> fetchItem(int id);
}

abstract class Cache {
  Future<int> addItem(ItemModel item);
  Future<int> clear();
}
