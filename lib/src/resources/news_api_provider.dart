import 'dart:convert';

import 'package:flutter_news/src/models/item_model.dart';
import 'package:flutter_news/src/repositories/repository.dart';
import 'package:http/http.dart' as http;

// Hacker News base url
final _hackerNewsBaseUrl = 'https://hacker-news.firebaseio.com/v0';

class NewsApiProvider implements Source {
  // using client for test perposes.
  // by doing this we can replace this with MockClient
  http.Client client = http.Client();

  Future<List<int>> fetchTopIds() async {
    final http.Response response = await client.get('$_hackerNewsBaseUrl/topstories.json');
    final List<dynamic> ids = json.decode(response.body);
    // cast item of list to a type we want. in this case dynamic -> int
    return ids.cast<int>();
  }

  Future<ItemModel> fetchItem(int id) async {
    final http.Response response = await client.get('$_hackerNewsBaseUrl/item/$id.json');
    final dynamic parsedJson = json.decode(response.body);
    final ItemModel item = ItemModel.fromJson(parsedJson);
    return item;
  }
}
