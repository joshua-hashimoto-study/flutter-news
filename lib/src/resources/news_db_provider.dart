import 'dart:async';
import 'dart:io';

import 'package:flutter_news/src/repositories/repository.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

import 'package:flutter_news/src/models/item_model.dart';

final String table = 'Items';

class NewsDbProvider implements Source, Cache {
  Database db;

  NewsDbProvider() {
    // to create/connect db
    init();
  }

  void init() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'items.db');
    db = await openDatabase(
      path,
      version: 1,
      onCreate: (Database newDb, int version) {
        String sql = """
        CREATE TABLE $table (
          id INTEGER PRIMARY KEY,
          deleted INTEGER,
          type TEXT,
          by TEXT,
          time INTEGER,
          text TEXT,
          dead INTEGER,
          parent INTEGER,
          kids BLOB,
          url TEXT,
          score INTEGER,
          title TEXT,
          descendants INTEGER
        );
        """;
        newDb.execute(sql);
      },
    );
  }

  Future<List<int>> fetchTopIds() {
    return null;
  }

  Future<ItemModel> fetchItem(int id) async {
    final List<Map<String, dynamic>> itemMaps = await db.query(
      table,
      columns: null,
      where: "id = ?",
      whereArgs: [id],
    );
    if (itemMaps.length > 0) {
      return ItemModel.fromDB(itemMaps.first);
    }
    return null;
  }

  Future<int> addItem(ItemModel item) {
    return db.insert(
      table,
      item.toMapForDb(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<int> clear() {
    return db.delete(table);
  }
}

// sqlite does not like opening connections to the same db,
// so make an instance once and referance this.
final newsDbProvider = NewsDbProvider();
