import 'package:flutter_news/src/models/item_model.dart';
import 'package:flutter_news/src/repositories/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';

class CommentsBloc {
  final Repository _repository = Repository();
  final _commentsFetcher = PublishSubject<int>();
  final _commentsOutput = BehaviorSubject<Map<int, Future<ItemModel>>>();

  // Streams
  Stream<Map<int, Future<ItemModel>>> get itemWithComments => _commentsOutput.stream;

  // Sinks
  Function(int) get fetchItemWithComments => _commentsFetcher.sink.add;

  CommentsBloc() {
    _commentsFetcher.stream.transform(_commentsTransformer()).pipe(_commentsOutput);
  }

  _commentsTransformer() {
    return ScanStreamTransformer<int, Map<int, Future<ItemModel>>>(
      (cache, int id, _) {
        cache[id] = _repository.fetchItem(id);
        cache[id].then(
          (ItemModel item) => {
            // recursive data fetching
            // pass the comment id to fetchItemWithComments(_commentsFetcher.sink.add) so
            // it restarts _commentsTransformer in the end. this will repeat till the end.
            // this is not really a best practice, but al least it works!
            item.kids.forEach(
              (kidId) => fetchItemWithComments(kidId),
            ),
          },
        );
        // do not forget to return the cache!
        // or it will not end!
        return cache;
      },
      <int, Future<ItemModel>>{},
    );
  }

  dispose() {
    _commentsFetcher.close();
    _commentsOutput.close();
  }
}
