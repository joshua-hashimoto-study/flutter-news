import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_news/src/blocs/comments_bloc.dart';
import 'package:flutter_news/src/models/item_model.dart';
import 'package:flutter_news/src/screens/widgets/comment_widget.dart';
import 'package:flutter_news/src/screens/widgets/loading_widget.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';

class NewsDetailScreen extends StatelessWidget {
  const NewsDetailScreen({
    @required this.itemOf,
  });

  final int itemOf;

  @override
  Widget build(BuildContext context) {
    final CommentsBloc _commentsBloc = Provider.of<CommentsBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail'),
      ),
      body: _buildBody(_commentsBloc),
    );
  }

  Widget _buildBody(CommentsBloc bloc) {
    return StreamBuilder(
      stream: bloc.itemWithComments,
      builder: (BuildContext context, AsyncSnapshot<Map<int, Future<ItemModel>>> snapshot) {
        if (snapshot.hasError) {
          return Container();
        }
        if (!snapshot.hasData) {
          return LoadingWidget();
        }
        final itemFuture = snapshot.data[itemOf];
        return FutureBuilder(
          future: itemFuture,
          builder: (BuildContext context, AsyncSnapshot<ItemModel> itemSnapshot) {
            if (itemSnapshot.hasError) {
              return Container();
            }
            if (!itemSnapshot.hasData) {
              return LoadingWidget();
            }
            return _buildPage(itemSnapshot.data, snapshot.data);
          },
        );
      },
    );
  }

  Widget _buildPage(ItemModel item, Map<int, Future<ItemModel>> itemMap) {
    final List<CommentWidget> commets = item.kids.map((kidId) {
      return CommentWidget(
        itemId: kidId,
        itemMap: itemMap,
        depth: 0,
      );
    }).toList();
    return ListView(
      children: [
        _buildTitle(item.title),
        _buildContent(item.text),
        ...commets,
      ],
    );
  }

  Widget _buildTitle(String title) {
    return Container(
      alignment: Alignment.topCenter,
      margin: EdgeInsets.all(10.0),
      child: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 20.0,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget _buildContent(String text) {
    return Container(
      alignment: Alignment.topCenter,
      margin: EdgeInsets.all(10.0),
      child: Html(
        data: text,
      ),
    );
  }
}
