import 'package:flutter/material.dart';
import 'package:flutter_news/src/blocs/stories_bloc.dart';
import 'package:flutter_news/src/screens/widgets/news_list_tile_widget.dart';
import 'package:flutter_news/src/screens/widgets/refresh_widget.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';

class NewsListScreen extends StatefulWidget {
  @override
  _NewsListScreenState createState() => _NewsListScreenState();
}

class _NewsListScreenState extends State<NewsListScreen> {
  StoriesBloc _storiesBloc;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _storiesBloc = Provider.of<StoriesBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Top News'),
      ),
      body: _buildStreamList(),
    );
  }

  Widget _buildStreamList() {
    return StreamBuilder(
      stream: _storiesBloc.topIds,
      builder: (BuildContext context, AsyncSnapshot<List<int>> snapshot) {
        if (snapshot.hasError) {
          return Container();
        }
        if (!snapshot.hasData) {
          return Center(
            child: SpinKitWanderingCubes(
              color: Colors.grey,
              size: 50.0,
            ),
          );
        }
        return RefreshWidget(
          child: ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext context, int index) {
              _storiesBloc.fetchItem(snapshot.data[index]);
              return NewsListTileWidget(
                itemId: snapshot.data[index],
              );
            },
          ),
        );
      },
    );
  }
}
