import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_news/src/models/item_model.dart';
import 'package:flutter_news/src/screens/widgets/loading_list_tile_widget.dart';
import 'package:flutter_news/src/screens/widgets/loading_widget.dart';

class CommentWidget extends StatelessWidget {
  const CommentWidget({
    this.itemId,
    this.itemMap,
    this.depth,
  });

  final int itemId;
  final Map<int, Future<ItemModel>> itemMap;
  final int depth;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: itemMap[itemId],
      builder: (BuildContext context, AsyncSnapshot<ItemModel> snapshot) {
        if (snapshot.hasError) {
          return Container();
        }
        if (!snapshot.hasData) {
          return LoadingListTileWidget();
        }
        final ItemModel item = snapshot.data;
        final children = [
          ListTile(
            title: _buildText(item.text),
            subtitle: item.by.isEmpty ? Text('This comment has been deleted.') : Text(item.by),
            contentPadding: EdgeInsets.only(
              right: 16.0,
              left: (depth + 1) * 16.0,
            ),
          ),
          Divider(),
        ];
        item.kids.forEach((kidId) {
          children.add(CommentWidget(
            itemId: kidId,
            itemMap: itemMap,
            depth: depth + 1,
          ));
        });

        return Column(
          children: children,
        );
      },
    );
  }

  Widget _buildText(String text) {
    return Html(data: text);
  }
}
