import 'package:flutter/material.dart';
import 'package:flutter_news/src/blocs/stories_bloc.dart';
import 'package:provider/provider.dart';

class RefreshWidget extends StatefulWidget {
  const RefreshWidget({
    this.child,
  });

  final Widget child;

  @override
  _RefreshWidgetState createState() => _RefreshWidgetState();
}

class _RefreshWidgetState extends State<RefreshWidget> {
  StoriesBloc _storiesBloc;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _storiesBloc = Provider.of<StoriesBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        await _storiesBloc.clearCache();
        await _storiesBloc.fetchTopIds();
      },
      child: widget.child,
    );
  }
}
