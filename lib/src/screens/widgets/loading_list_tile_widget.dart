import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class LoadingListTileWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: Colors.grey.shade300,
      highlightColor: Colors.white,
      child: Column(
        children: [
          ListTile(
            title: _buildBox(),
            subtitle: _buildBox(),
          ),
          Divider(
            height: 8.0,
          ),
        ],
      ),
    );
  }

  Widget _buildBox() {
    return Container(
      color: Colors.grey.shade300,
      height: 24.0,
      width: 150,
      margin: EdgeInsets.only(
        top: 5.0,
        bottom: 5.0,
      ),
    );
  }
}
