import 'package:flutter/material.dart';
import 'package:flutter_news/src/blocs/stories_bloc.dart';
import 'package:flutter_news/src/models/item_model.dart';
import 'package:flutter_news/src/screens/widgets/loading_list_tile_widget.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';

class NewsListTileWidget extends StatefulWidget {
  const NewsListTileWidget({
    this.itemId,
  });

  final int itemId;

  @override
  _NewsListTileWidgetState createState() => _NewsListTileWidgetState();
}

class _NewsListTileWidgetState extends State<NewsListTileWidget> {
  StoriesBloc _storiesBloc;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _storiesBloc = Provider.of<StoriesBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    _storiesBloc = Provider.of<StoriesBloc>(context);
    return StreamBuilder(
      stream: _storiesBloc.items,
      builder: (BuildContext context, AsyncSnapshot<Map<int, Future<ItemModel>>> snapshot) {
        if (snapshot.hasError) {
          return Container();
        }
        if (!snapshot.hasData) {
          // return SpinKitRing(color: Colors.black);
          // return Text('Loading.... ${widget.itemId}');
          return LoadingListTileWidget();
        }
        return FutureBuilder(
          future: snapshot.data[widget.itemId],
          builder: (BuildContext context, AsyncSnapshot<ItemModel> itemSnapshot) {
            if (itemSnapshot.hasError) {
              return Container();
            }
            if (!itemSnapshot.hasData) {
              // return SpinKitRing(color: Colors.black);
              // return Text('Still Loading.... ${widget.itemId}');
              return LoadingListTileWidget();
            }
            return _buildTile(context, item: itemSnapshot.data);
          },
        );
      },
    );
  }

  Widget _buildTile(BuildContext context, {ItemModel item}) {
    return Column(
      children: [
        ListTile(
          onTap: () {
            Navigator.pushNamed(context, '/${item.id}');
          },
          title: Text(item.title),
          subtitle: Text('${item.score} votes'),
          trailing: Column(
            children: [
              Icon(Icons.comment),
              Text('${item.descendants}'),
            ],
          ),
        ),
        Divider(
          height: 8.0,
        ),
      ],
    );
  }
}
